# Persona Souls Compendium

## Getting started

**Start database docker container:**

docker-compose up

**Start virtual environmet:**

source venv/bin/activate

**Star backend (in personasoulscompendiu folder):**

python manage.py runserver 8888

**Star frontend (in frontend folder):**

npm run serve

Back office: [http://localhost:8888/admin](http://localhost:8888/admin)
