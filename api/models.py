from distutils.command.upload import upload
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Character(models.Model):
    name = models.CharField(max_length=50)
    persona = models.CharField(max_length=50,blank=True,null=True)
    hp = models.IntegerField()
    attack = models.IntegerField()
    defense = models.IntegerField()
    speed = models.IntegerField()
    game = models.CharField(max_length=50)
    characterImage = models.ImageField(blank=True,null=True,upload_to="cards/characters")
    personaImage = models.ImageField(blank=True,null=True,upload_to="cards/personas")
    
    def __str__(self):
        return self.name

class Profile(models.Model):
    id = models.BigIntegerField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cardShards = models.IntegerField()
    userImage = models.ImageField(blank=True,null=True)

class Party(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    

class Boss(models.Model):
    name = models.CharField(max_length=50)
    hp = models.IntegerField()
    attack = models.IntegerField()
    defense = models.IntegerField()
    speed = models.IntegerField()
    game = models.CharField(max_length=50)
    bossImage = models.ImageField(blank=True,null=True,upload_to="bosses")
    class Meta:
        verbose_name_plural = "bosses"
    
    def __str__(self):
        return self.name

class Game(models.Model):
    name = models.CharField(max_length=50)
    platform = models.CharField(max_length=50)
    release_date_jp = models.DateField(blank=True,null=True)
    release_date_eeuu = models.DateField(blank=True,null=True)
    release_date_eu = models.DateField(blank=True,null=True)
    synopsis = models.TextField()
    image = models.ImageField(blank=True,null=True,upload_to="games")

    def __str__(self):
        return self.name

class Anime(models.Model):
    name = models.CharField(max_length=50)
    synopsis = models.TextField()
    studio = models.CharField(max_length=50,blank=True,null=True)
    episodes = models.IntegerField(blank=True,null=True)
    image  = models.ImageField(blank=True,null=True,upload_to="anime")
    
    def __str__(self):
        return self.name

class Manga(models.Model):
    name = models.CharField(max_length=50)
    synopsis = models.TextField()
    author = models.CharField(max_length=50,blank=True,null=True)
    volumes = models.IntegerField(blank=True,null=True)
    image = models.ImageField(blank=True,null=True,upload_to="manga")
    
    def __str__(self):
        return self.name