# from django.shortcuts import render
import re
from rest_framework import viewsets
from rest_framework.response import Response
from api.models import Character, Profile, Boss, Game ,Anime, Manga,Party,User
from .serializer import CharacterSerializer, ProfileSerializer ,BossSerializer, GameSerializer ,AnimeSerializer, MangaSerializer, PartySerializer,UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer

    def get_queryset(self):
        user = User.objects.all()
        return user

class CharacterViewSet(viewsets.ModelViewSet):
    serializer_class = CharacterSerializer

    def get_queryset(self):
        character = Character.objects.all()
        return character

class ProfileViewSet(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer

    def get_queryset(self):
        profile = Profile.objects.all()
        return profile

class PartyViewSet(viewsets.ModelViewSet):
    serializer_class = PartySerializer

    def get_queryset(self):
        party = Party.objects.all()
        return party
        
    def retrieve(slef,request,*args,** kwargs):
        params=kwargs
        print(params['pk'])
        partyMember = Party.objects.filter(profile=params['pk'])
        serializer=PartySerializer(partyMember,many=True)
        return Response(serializer.data) 

class BossViewSet(viewsets.ModelViewSet):
    serializer_class = BossSerializer

    def get_queryset(self):
        boss = Boss.objects.all()
        return boss

class GameViewSet(viewsets.ModelViewSet):
    serializer_class = GameSerializer

    def get_queryset(self):
        game = Game.objects.all()
        return game

class AnimeViewSet(viewsets.ModelViewSet):
    serializer_class = AnimeSerializer

    def get_queryset(self):
        anime = Anime.objects.all()
        return anime

class MangaViewSet(viewsets.ModelViewSet):
    serializer_class = MangaSerializer

    def get_queryset(self):
        manga = Manga.objects.all()
        return manga
