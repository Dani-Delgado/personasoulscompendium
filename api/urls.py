from django.urls import include, re_path
from .views import CharacterViewSet, ProfileViewSet, BossViewSet, GameViewSet, AnimeViewSet, MangaViewSet, PartyViewSet,UserViewSet
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register('character', CharacterViewSet, basename="character")
router.register('profile', ProfileViewSet, basename="profile")
router.register('boss', BossViewSet, basename="boss")
router.register('game', GameViewSet, basename="game")
router.register('anime', AnimeViewSet, basename="anime")
router.register('manga', MangaViewSet, basename="manga")
router.register('party', PartyViewSet, basename="party")
router.register('user', UserViewSet, basename="user")

urlpatterns = [
    re_path('', include(router.urls))
]

