from django.contrib import admin
from api.models import Character, Profile, User, Boss, Game, Anime, Manga, Party

# Register your models here.

class CharacterAdmin(admin.ModelAdmin):
    list_display=("name", "persona", "game", "hp", "attack", "defense", "speed", "characterImage", "personaImage")
    list_filter=("game",)

class ProfileAdmin(admin.ModelAdmin):
    list_display=("id","user", "cardShards", "userImage")


class PartyAdmin(admin.ModelAdmin):
    list_display=("id","profile", "character")

class BossAdmin(admin.ModelAdmin):
    list_display=("name", "game", "hp", "attack", "defense", "speed", "bossImage")
    list_filter=("game",)

class GameAdmin(admin.ModelAdmin):
    list_display=("name", "platform", "synopsis", "release_date_jp", "release_date_eeuu", "release_date_eu", "image")
    list_filter=("platform",)

class AnimeAdmin(admin.ModelAdmin):
    list_display=("name","studio","episodes","synopsis", "image")

class MangaAdmin(admin.ModelAdmin):
    list_display=("name", "author", "volumes", "synopsis", "image")

admin.site.register(Character, CharacterAdmin)

admin.site.register(Profile, ProfileAdmin)

admin.site.register(Boss, BossAdmin)

admin.site.register(Game, GameAdmin)

admin.site.register(Anime, AnimeAdmin)

admin.site.register(Manga, MangaAdmin)

admin.site.register(Party, PartyAdmin)