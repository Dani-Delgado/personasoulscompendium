import { createStore } from 'vuex'

export default createStore({
    state:{
        access: '',
        refresh: ''
    },
    mutations: {
        initializeStore(state) {
            if ( localStorage.getItem("access")) {
                state.access = localStorage.getItem("access")
                state.refresh = localStorage.getItem("refresh")
            } else {
                state.access = ''
                state.refresh = ''
            }
        },
        setAccess(state, access){
            state.access = access
        },
        setRefresh(state, refresh){
            state.refresh = refresh
        },
        setLogout(state) {
            state.access = '';
            state.refresh = '';
            state.userdata = '';
            state.isAuthenticated = false;
          },
    },
    actions: {
        userLogout(){
            state.accessToken = null
            state.refreshToken = null
        },
        checkLogin() {
            if(state.accessToken && state.refreshToken){
                return True
            } else {
                return False
            }
        }

    },
    modules: {

    }
})