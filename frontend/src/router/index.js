import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import SignUp from '@/views/SignUp.vue'
import LogIn from '@/views/LogIn.vue'
import Logout from '@/views/Logout.vue'
import ListCharacter from '@/views/Character/ListCharacter'
import ListBosses from '@/views/Bosses/ListBosses'
import UserCharacters from '@/views/Character/UserCharacters'
import Games from '@/views/Games/Games'
import Manga from '@/views/Manga/manga'
import Anime from '@/views/Anime/anime'
import Fight from '@/views/Fight/fight'
import Gatcha from '@/views/Gatcha/gatcha.vue'
import ChooseCharacter from '@/views/Fight/chooseCharacter.vue'
import Summon from '@/views/Character/summon.vue'
import PageNotFound from '@/views/pageNotFound.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },

  {
    path: '/sign-up',
    name: 'SignUp',
    component: SignUp
  },
  {
    path: '/login',
    name: 'LogIn',
    component: LogIn
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
    beforeEnter: (to, from, next) => {
      if(!localStorage.getItem("access")){
        return next({
          name:'LogIn'
        })
      }

      next()
    }
  },
  {
    path: '/bosses',
    name: 'ListBosses',
    component: ListBosses
  },
  {
    path: '/character',
    name: 'ListCharacter',
    component: ListCharacter
  },
  {
    path: '/usercharacters',
    name: 'UserCharacters',
    component: UserCharacters,
    beforeEnter: (to, from, next) => {
      if(!localStorage.getItem("access")){
        return next({
          name:'LogIn'
        })
      }

      next()
    }
  },
  {
    path: '/games/:gameId',
    name: 'Games',
    component: Games
  },
  {
    path: '/manga/:mangaId',
    name: 'Manga',
    component: Manga
  },
  {
    path: '/anime/:animeId',
    name: 'Anime',
    component: Anime
  },
  {
    path: '/gatcha',
    name: 'Gatcha',
    component: Gatcha,
    beforeEnter: (to, from, next) => {
      if(!localStorage.getItem("access")){
        return next({
          name:'LogIn'
        })
      }

      next()
    }
  },
  {
    path: '/summon/:id',
    name: 'Summon',
    component: Summon,
    beforeEnter: (to, from, next) => {
      if(!localStorage.getItem("access")){
        return next({
          name:'LogIn'
        })
      }

      next()
    }
  },
  {
    path: '/fight',
    name: 'ChooseCharacter',
    component: ChooseCharacter,
    beforeEnter: (to, from, next) => {
      if(!localStorage.getItem("access")){
        return next({
          name:'LogIn'
        })
      }

      next()
    }
  },
  {
    path: '/fight/:characterid/:bossid',
    name: 'Fight',
    component: Fight,
    beforeEnter: (to, from, next) => {
      if(!localStorage.getItem("access")){
        return next({
          name:'LogIn'
        })
      }

      next()
    }
  },
  {
    path: '/:path',
    name: '/:pathMatch(.*)*',
    component: PageNotFound
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
