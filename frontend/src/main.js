import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import './assets/tailwind.css'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { faInstagram, faGitlab, faLinkedinIn, faTwitter } from '@fortawesome/free-brands-svg-icons'

axios.defaults.baseURL = 'http://localhost:8888'

library.add(fas, faInstagram, faGitlab, faLinkedinIn, faTwitter)

createApp(App).use(store).use(router).component('fa', FontAwesomeIcon).mount('#app')


