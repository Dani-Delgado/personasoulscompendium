module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        velvetroom: {
          50: '#9ab1f5',
          100: '#799afc',
          200: '#557efa',
          300: '#3867f5',
          400: '#1e54f7',
          500: '#0240fa',
          600: '#0439d6',
          700: '#00279c',
          800: '#011c6e',
          900: '#001040',
        },
      },
    },
    fontSize: {
      'xs': '.75rem',
      'sm': '.875rem',
      'tiny': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
    }
  },
  plugins: [],
}
